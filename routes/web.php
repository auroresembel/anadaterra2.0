<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ArticleController@index');

Route::get('details-choco/{id}', 'ArticleController@show');

Route::get('/home', function () {
    return view('home');
});

Route::get('/mes_reservations', function () {
    return view('mes_reservations');
});

Route::get('/chocolats', 'ArticleController@seeAllChocolats');

Route:: get('/a_propos', function () {
    return view('a_propos');
});

Route::get('/reservation', function () {
    return view('reservation/cart');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// VOIR TOUT LES PRODUITS EN TANT QU'ADMIN (CRUD)
Route::get('/index', 'ArticleController@index2');

Route::resource('admin', 'ArticleController');

