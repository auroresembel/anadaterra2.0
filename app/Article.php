<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = [
        'id',
        'name',
        'unit_price',
        'image',
        'description',
        'created-at',
        'updated-at'
    ];

    public $timestamps = false;
}
