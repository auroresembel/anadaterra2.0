@include('components/header')
<main>
    <section class="intro d-flex align-items-center justify-content-center text-center">
        <div>
            <h2 class="light">CHOCOLAT CRU ARIEGE</h2>
            <h3 class="light">L'aliment des dieux</h3>
            <h1>BIENVENUE</h1>
        </div>
    </section>


<section class="container-card carousel slide" data-ride="carousel" id="carouselExampleIndicators">
    <ol class="carousel-indicators none-mobile">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
    </ol>
    <div class="container carousel-inner">
        <div class="carousel-item active">
            <div class="col flex-desktop">
            @foreach ($articles->slice(0,2) as $article)
                <div class="card text-center" style="width: 22rem;">
                    <div class="card-body">
                        <h5 class="card-title ">{{ $article->name}}</h5>
                        <img src="{{url($article->image)}}" alt=""><!-- IMAGE DU PRODUITS A EXPORTER DEPUIS LA BBD -->
                        <p class="card-text">{{ $article->description}}</p>
                        <a href="{{ url('details-choco', ['article'=>$article->id]) }}" class="btn btn-primary btn-absolute">VOIR LE PRODUIT</a>
                    </div>
                </div>
            @endforeach
            </div>
        </div>
        <div class="carousel-item">
            <div class="col flex-desktop">
            @foreach ($articles->slice(2,2) as $article)
                <div class="card text-center" style="width: 22rem;">
                    <div class="card-body">
                        <h5 class="card-title ">{{ $article->name}}</h5>
                        <img src="{{url($article->image)}}" alt=""><!-- IMAGE DU PRODUITS A EXPORTER DEPUIS LA BBD -->
                        <p class="card-text">{{ $article->description}}</p>
                        <a href="{{ url('details-choco', ['article'=>$article->id]) }}" class="btn btn-primary btn-absolute">VOIR LE PRODUIT</a>
                    </div>
                </div>
            @endforeach
            </div>
        </div>
        <div class="carousel-item">
            <div class="col flex-desktop">
            @foreach ($articles->slice(4,2) as $article)
                <div class="card text-center" style="width: 22rem;">
                    <div class="card-body">
                        <h5 class="card-title ">{{ $article->name}}</h5>
                        <img src="{{url($article->image)}}" alt=""><!-- IMAGE DU PRODUITS A EXPORTER DEPUIS LA BBD -->
                        <p class="card-text">{{ $article->description}}</p>
                        <a href="{{ url('details-choco', ['article'=>$article->id]) }}" class="btn btn-primary btn-absolute">VOIR LE PRODUIT</a>
                    </div>
                </div>
            @endforeach
            </div>
        </div>

        <div class="carousel-item">
            <div class="col flex-desktop">
            @foreach ($articles->slice(6,2) as $article)
                <div class="card text-center" style="width: 22rem;">
                    <div class="card-body">
                        <h5 class="card-title ">{{ $article->name}}</h5>
                        <img src="{{url($article->image)}}" alt=""><!-- IMAGE DU PRODUITS A EXPORTER DEPUIS LA BBD -->
                        <p class="card-text">{{ $article->description}}</p>
                        <a href="{{ url('details-choco', ['article'=>$article->id]) }}" class="btn btn-primary btn-absolute">VOIR LE PRODUIT</a>
                    </div>
                </div>
            @endforeach
            </div>
        </div>
        <div class="carousel-item">
            <div class="col flex-desktop">
            @foreach ($articles->slice(8,2) as $article)
                <div class="card text-center" style="width: 22rem;">
                    <div class="card-body">
                        <h5 class="card-title ">{{ $article->name}}</h5>
                        <img src="{{url($article->image)}}" alt=""><!-- IMAGE DU PRODUITS A EXPORTER DEPUIS LA BBD -->
                        <p class="card-text">{{ $article->description}}</p>
                        <a href="{{ url('details-choco', ['article'=>$article->id]) }}" class="btn btn-primary btn-absolute">VOIR LE PRODUIT</a>
                    </div>
                </div>
            @endforeach
            </div>
        </div>
    </div>
    <a class="carousel-control-prev none-mobile" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <i class="fa fa-lg fa-chevron-left custom-btn-carousel"></i>
    </a>
    <a class="carousel-control-next none-mobile" href="#carouselExampleIndicators" role="button" data-slide="next">
        <i class="fa fa-lg fa-chevron-right custom-btn-carousel"></i>
    </a>
</section>


    <section>
        <div class="cacaoseed-container">
            <div class="cacaoseed">
            </div>
            <div class="text-start cacaoseed-content d-flex justify-content-center align-items-center">
                <div>
                    <h3 class="text-center">C'EST QUOI LE CHOCOLAT CRU ?</h3>
                    <p>C'est du chocolat à base de cacao, de beurre de cacao mélangé avec de l'huile de coco accompagné de miel ou de dattes pour attenuer l'amertume du cacao. Ce qui change en comparaison au chocolat "normal" c'est qu'aucun des ingrédients n'est chauffé à plus de 42° ainsi que l'absence de produits animaliers (excepté le miel).</p>
                </div>
            </div>
        </div>
        <div class="cacaoseed-container cacaoseed-container2">
            <div class="text-start cacaoseed-content d-flex justify-content-center align-items-center">
                <div>
                    <h3 class="text-center">POURQUOI CRU ?</h3>
                    <p>C'est simplement afin de conserver tous les bienfaits des composants présents dans les ingrédients cités des bienfaits tels que la présence de puissants antioxydants qui empêchent nos cellules de vieillir prématurément. Nous pourrions également citer les multiples vitamines bénéfiques à notre corps... En plus des bienfaits intenses pour notre corps, la synergie des goûts  que procurent les ingrédients crus sont très savoureux et restent plus longtemps en bouche. ...Mais à quoi bon intellectualiser une telle chose ? Découvrez la différence par vous-même !</p>
                </div>
            </div>
            <div class="cacaoseed">
            </div>
        </div>
    </section>

    <section class="text-center brown-container">
        <h1>LE SAVIEZ-VOUS ?</h1>
        <p>Théobromine est un mot dérivé de "Theobroma", nom générique du cacaoyer composé des racines grecques Theo (dieu) et broma (nourriture) signifiant "Nourriture des dieux". Rapprochez vous de votre nature divine en consommant du chocolat cru ;) </p>
    </section>

    <section class="text-center services-container">
        <div class="row">
            <div class="col">
                <i class="far fa-hand-pointer fa-5x"></i>
                <h1>Sélection</h1>
            </div>
            <div class="col">
                <i class="far fa-calendar-check fa-5x"></i>
                <h1>Réservation</h1>  
            </div>
            <div class="col">
                <i class="fas fa-people-carry fa-5x"></i>
                <h1>Livraison</h1>
            </div>
        </div>
    </section>




    <section class="brown-container2">
        <h1 class="text-center">LES INGREDIENTS</h1>
        <div class="d-flex flex-wrap">
            <div class="col-12 text-center" style="margin-top:15px;">
                <h2>Poudre et beurre de cacao</h2>
                <p>La poudre et le beurre de cacao que nous utilisons proviennent d'un ferme familiale située au Pérou. Certifiée AB, elle possède également le label commerce équitable et vegan.</p>
            </div>
            <div class=" col-6 text-center" style="margin-top:15px;">
                <h2>Miel</h2>
                <p>Miel biologique de préférence issu de producteurs locaux.</p>
            </div>
            
            <div class="col-6 text-center" style="margin-top:15px;">
                <h2>Dattes</h2>
                <p>Pâte de datte biologiques d'origine Tunisienne.</p>
            </div>
            <div class="col-12 text-center" style="margin-top:15px;">
                <h2>Huile de coco</h2>
                <p>Produit biologique d'origine Philippine.</p>
            </div>
        </div>
    </section>

    
</main>


@include('components/footer')
<script>
   (function($){
	   "use strict";
	   $('.next').click(function(){ $('.carousel').carousel('next');return false; });
	   $('.prev').click(function(){ $('.carousel').carousel('prev');return false; });
   })	
   (jQuery);
</script>    