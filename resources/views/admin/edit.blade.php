@extends('layouts.app')

@section('content')

<div class="card uper">
  <div class="card-header">
    Modifier le produit
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('admin.update', $article->id) }}">
        @method('PATCH')
        @csrf
        <div class="form-group">
          <label for="name">Nom du produit:</label>
          <input type="text" class="form-control" name="name" value={{ $article->name }} />
        </div>
        <div class="form-group">
          <label for="price">Description du produit :</label>
          <input type="text" class="form-control" name="description" value={{ $article->description }} />
        </div>
        <div class="form-group">
          <label for="price">Url de l'image :</label>
          <input type="text" class="form-control" name="image" value={{ $article->image }} />
        </div>
        <div class="form-group">
          <label for="price">Prix a l'unité :</label>
          <input type="text" class="form-control" name="price" value={{ $article->price }} />
        </div>
        <button type="submit" class="btn btn-primary">Mettre à jour</button>
      </form>
  </div>
</div>
@endsection