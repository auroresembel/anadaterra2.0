@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card card-login-register">
                
                <div class="card-body card-body-login-register">
                    <div class="left-card">
                        <img src="../../public/images/600.png" alt="">
                    </div>
                    <form method="POST" action="{{ route('register') }}" class="right-card">
                        @csrf
                        <h3 class="text-center">Bienvenue !</h3>
                        <p class="col">Veuillez vous inscrire <br>pour plus d'aventure !</p>

                        <div class="form-group">
                            <label for="prenom" class="col">{{ __('Prenom') }}</label>

                            <div class="col">
                                <input id="prenom" type="text" class="form-control @error('prenom') is-invalid @enderror" name="prenom" value="{{ old('prenom') }}" required autocomplete="prenom" autofocus>
                                
                                @error('prenom')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                                <label for="nom" class="col">{{ __('Nom') }}</label>
    
                                <div class="col">
                                    <input id="nom" type="text" class="form-control @error('nom') is-invalid @enderror" name="nom" value="{{ old('nom') }}" required autocomplete="nom" autofocus>
    
                                    @error('nom')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>


                        <div class="form-group">
                            <label for="email" class="col">{{ __('E-mail') }}</label>

                            <div class="col">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                                <label for="phone" class="col">{{ __('Numéro de téléphone') }}</label>
    
                                <div class="col">
                                    <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone" autofocus>
                                    
                                    @error('phone')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                        <div class="form-group">
                            <label for="password" class="col">{{ __('Mot de passe') }}</label>

                            <div class="col">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col">{{ __('Confirmation du mot de passe') }}</label>

                            <div class="col">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('S\'enregistrer') }}
                                </button>
                            </div>
                        </div>

                        <p class="d-flex justify-content-center">Vous êtes déjà membre ?</p>
                        <a class="btn btn-link text-center d-flex justify-content-center link-choco" href="{{ route('login') }}">Connectez vous ici !</a>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
