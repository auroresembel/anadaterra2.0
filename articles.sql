

--
-- Contenu de la table `articles`
--

INSERT INTO `articles` (`id`, `name`, `price`, `image`, `description`, `created_at`, `updated_at`) VALUES
(1, 'chococolat au miel', '0,50€', 'images/10_miel.jpg', 'Le chococolat au miel vous présente un délicieux mélange entre la douceur du miel ariégeois et l’amertume du cacao premium composé de 75 % de fèves Criollo et de 25 % Forastero et Triniario.Ce chococolat a une dominance amère.', NULL, NULL),
(2, 'chococolat miel-café', '0,05€', 'images/10_miel-café.jpg', 'Le chococolat miel-café est pour les ama-teurs d’amertume et de croquant. Les deux grains de café complémentent l’amertume intense du chococolat au miel et contrastent son côté fondant par leur croquant soutenu.', NULL, NULL),
(3, 'chococolat miel-goji', '0,50€', 'images/10_miel-goji.jpg', 'Le chococolat miel-goji incorpore 2-3 de ces petites baies extraordinaires chargées en vitamines, acides aminés, minéraux et oligo éléments. Ce petit chococolat est parfait comme un en-cas énergisant, pour bien démarrer une journée ou simplement pour une pause gourmande.', NULL, NULL),
(4, 'chococolat à la datte', '0,50€', 'images/10_dattes.jpg', 'Le chococolat à la datte est fabriqué en faisant tremper de la pâte de datte dans de l’eau de rose. Avec une proportion de poudre de cacao moins élevée que celui au miel, le chococolat à la datte est idéal pour les amateurs de chocolat peu amère. La pré-sence de l’eau de rose se révèlera par une douceur fleurie aux palais sensibles. ', NULL, NULL),
(5, 'chococolat blanc', '0,50€', 'images/10_blanc.jpg', 'Le chococolat blanc est un subtile mélange de saveurs entre la farine de coco, la purée d’amande blanche et noix de cajou broyées. Il est le plus doux de tous, cependant il est incomparable avec le chocolat blanc traditionnel en raison de l’absence de poudre de lait ajoutée. Doux et nourrissant il est parfait pour les palais qui ne raffolent pas de l’amertume du cacao.*', NULL, NULL),
(6, 'chococolat blanc-café', '0,50€', 'images/10_blanc-café.jpg', 'Le chococolat blanc-café célèbre la douceur du chococolat blanc et se marie parfaitement avec deux grains de café, croquants à souhait. *', NULL, NULL),
(7, 'chococolat à la menthe', '0,50€', 'images/10_menthe.jpg', 'Le chococolat à la menthe est un chococolat assez doux mais très raffraichissant !*', NULL, NULL),
(8, 'chococolat au gingembre', '0,50€', 'images/10_gingembre.jpg', 'e chococolat au gingembre est parfait pour les amateurs du piquant revigorant du gingembre. Il est parsemé de fin morceaux de gingembre grillés.*', NULL, NULL),
(9, 'chococolat au citron ', '0,50€', 'images/10_citron.jpg', 'Le chococolat au citron présente une synergie surprenante entre le cacao et le citron, malgré le fait qu’il contient autant de sirop d’agave cru que ses cousins aromatisés, on a l’impression qu’il est moins doux. Il est agréablement rafraichissant et il est couvert d’un fin zeste de citron.*', NULL, NULL),
(10, 'chococlat orange-cannelle', '0,50€', 'images/10_orange-cannelle.jpg', 'Le chococlat orange-cannelle vous fera surement penser à noël ou à du pain d’épice. Malgré cela, l’orange, la cannelle et le cacao aux discrètes touches de coco s’assemblent si bien qu’on peut en profiter tout au long de l’année.*', NULL, NULL);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
